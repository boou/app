import { TextStyle, ViewStyle } from "react-native"
import { color, typography, borderRadius, borderWidth, spacing } from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE: TextStyle = {
  fontFamily: typography.primary,
  color: color.palette.white,
  minHeight: 15,
  fontSize: 15,
}


const ROUNDED: ViewStyle = {
  backgroundColor: `${color.primaryDarker}77`,
  borderRadius: borderRadius.full,
  borderColor: color.palette.white,
  borderWidth: borderWidth[1],
  height: spacing[7]
}

/**
 * All the variations of button styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const buttonPresets = {
  default: {},

  login: {
    borderBottomColor: color.palette.white,
    borderBottomWidth: borderWidth[1]
  } as ViewStyle,

  /*
  * Rounded style
  */
  rounded: ROUNDED,

  darkblue: ROUNDED,


  roundedLighter: {
    ...ROUNDED,
    backgroundColor: color.palette.white,
  } as ViewStyle,
}

export const textPresets = {
  default: {
    backgroundColor: color.transparent,
  },

  login: {
    fontFamily: typography.subheader,
    color: color.text,
  } as TextStyle,

  rounded: {
    ...BASE,
    fontFamily: typography.subheader,
  } as TextStyle,

  darkblue: {
    ...BASE,
    color: color.palette.darkBlue
  } as TextStyle,
}
/**
 * A list of preset names.
 */
export type ButtonPresets = keyof typeof buttonPresets
