import * as React from "react"
import { View, TextStyle, ViewStyle, Picker as ReactPicker } from "react-native"
import { spacing, borderWidth, color } from "../../theme"
import { PickerProps } from "./picker.props"
import { mergeAll, flatten } from "ramda"
import { buttonPresets, textPresets } from "./picker.presets";

// the base styling for the container
const CONTAINER: ViewStyle = {
  borderBottomWidth: borderWidth[1],
  borderBottomColor: color.primary,
}

// the base styling for the TextInput
const INPUT: TextStyle = {
}

const enhance = (style, styleOverride) => {
  return mergeAll(flatten([style, styleOverride]))
}


/**
 * A component which has a label and an input together.
 */
export class Picker extends React.Component<PickerProps, {}> {
  render() {
    const { t } = this.props;

    const {
      placeholderTx,
      placeholder,
      selectedValue,
      values,
      labels,
      items,
      preset = "default",
      style: styleOverride,
      inputStyle: inputStyleOverride,
      forwardedRef,
      ...rest
    } = this.props
    let containerStyle: ViewStyle = { ...CONTAINER, ...buttonPresets[preset] }
    containerStyle = enhance(containerStyle, styleOverride)

    let inputStyle: TextStyle = { ...INPUT, ...textPresets[preset] }
    inputStyle = enhance(inputStyle, inputStyleOverride)
    const actualPlaceholder = placeholderTx ? t(placeholderTx) : placeholder

    return (
      <View style={containerStyle}>
        <ReactPicker
          prompt={actualPlaceholder}
          selectedValue={selectedValue}
          style={inputStyle}
          itemStyle={inputStyle}
          ref={forwardedRef}
          mode="dropdown"
          {...rest}
        >
          {items.map(({ label, value }, index) =>
            <ReactPicker.Item key={index} label={label} value={value} />
          )}
        </ReactPicker>
      </View >
    )
  }
}


