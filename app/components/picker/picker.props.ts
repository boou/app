import { TextStyle, ViewStyle, PickerProps as ReactPickerProps } from "react-native"
import i18next from 'i18next';

export interface PickerProps extends ReactPickerProps {

  /**
   * The placeholder i18n key.
   */
  placeholderTx?: string

  /**
   * The Placeholder text if no placeholderTx is provided.
   */
  placeholder?: string

  /**
   * Value matching value of one of the items.
   * Can be a string or an integer.
   */
  selectedValue?: any

  /**
 * Values of the items.
 * Can be strings or integers.
 */
  values?: Array<any>


  /**
   * Labels of the items.
   * Can be strings or integers.
   */
  labels?: Array<string>

  /**
   * Items, can be used instead values and labels arrays.
   * Can be strings or integers.
   */
  items?: Array<{ label: string, value: any }>

  /**
   * Optional container style overrides useful for margins & padding.
   */
  style?: ViewStyle | ViewStyle[]

  /**
   * Optional style overrides for the input.
   */
  inputStyle?: TextStyle | TextStyle[]

  /**
   * Various look & feels.
   */
  preset?: "default"

  forwardedRef?: any

  t?: i18next.TFunction
  i18n?: i18next.i18n
}
