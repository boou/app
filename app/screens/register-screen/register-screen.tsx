import * as React from "react"
import { observer } from "mobx-react"
import { observable } from 'mobx'
import { ViewStyle, TextStyle, View, StatusBar, SafeAreaView } from "react-native"
import { Text } from "../../components/text"
import { Button } from "../../components/button"
import { Screen } from "../../components/screen"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Wallpaper } from "../../components/wallpaper";
import { Header } from "../../components/header";

export interface RegisterScreenProps extends NavigationScreenProps<{}> {
}

/**
 * Styles
 */

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  // backgroundColor: color.palette.black,
  paddingHorizontal: spacing[4],
}

const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[5] - 1,
  paddingHorizontal: 0,
}

const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}

/**
 * Screen Component
 */

// @inject("mobxstuff")
@observer
export class RegisterScreen extends React.Component<RegisterScreenProps, {}> {
  // Reactive variables (Mobx)
  @observable count = 1

  _goBack = () => this.props.navigation.goBack(null)
  
  _clickButton = () => this.count++

  /**
   * function for optain data from API (is a React service)
   */
  _getData() {
    let data = {
      count: 1,
      text: "test",
      label: "Example hardcoding text"
    }
    return data
  }


  render () {

    const data = this._getData()

    return (
      <View style={FULL}>
        <StatusBar barStyle="light-content" />
        <Wallpaper />
        <SafeAreaView style={FULL}>
          <Screen style={CONTAINER} backgroundColor={color.transparent} preset="fixed">
            <Header
              headerTx="registerScreen.head"
              leftIcon="back"
              // preset="float" // other presets: default (same if you don't put preset attr)
              onLeftPress={this._goBack}
              style={HEADER}
              titleStyle={HEADER_TITLE}
            />

            <Text preset="header" tx="registerScreen.header" />
            <Text  preset="secondary"  text={String(this.count)}/>

            <Button 
              onPress={this._clickButton}
              text={data.label}
              
            />
          </Screen>
        </SafeAreaView>
      </View>
    )
  }
}
