import * as React from "react"
import { observer } from "mobx-react"
import { observable } from 'mobx'
import { ViewStyle, TextStyle, View, StatusBar, SafeAreaView } from "react-native"
import { Text } from "../../components/text"
import { Button } from "../../components/button"
import { Screen } from "../../components/screen"
import { color, spacing, borderWidth } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Wallpaper } from "../../components/wallpaper";
import { Header } from "../../components/header";
import { Picker } from "../../components/picker";
import { TextField } from "../../components/text-field";

export interface SpecificDataScreenProps extends NavigationScreenProps<{}> {
}

/**
 * Styles
 */

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  // backgroundColor: color.palette.black,
  paddingHorizontal: spacing[4],
}

const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[5] - 1,
  paddingHorizontal: 0,
}

const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}

const ROW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  marginVertical: spacing[1],
}

const ROW_LABEL: TextStyle = {
  flexGrow: 1
}

const ROW_INPUT: ViewStyle = {
  width: 150,
}
/**
 * Screen Component
 */

// @inject("mobxstuff")
@observer
export class SpecificDataScreen extends React.Component<SpecificDataScreenProps, {}> {
  // Reactive variables (Mobx)
  @observable count = 1

  _goBack = () => this.props.navigation.goBack(null)

  _clickButton = () => this.count++

  /**
   * function for optain data from API (is a React service)
   */
  _getTarjetas() {
    let data = [
      { label: "bancolombia", value: "Bancolombia" },
      { label: "itau", value: "Itaú" }
    ]
    return data
  }

  render() {

    const tarjeta = this._getTarjetas()

    return (
      <View style={FULL}>
        <StatusBar barStyle="light-content" />
        <Wallpaper />
        <SafeAreaView style={FULL}>
          <Screen style={CONTAINER} backgroundColor={color.transparent} preset="fixed">
            <Header
              headerText="Detalle"
              leftIcon="back"
              onLeftPress={this._goBack}
              style={HEADER}
              titleStyle={HEADER_TITLE}
            />

            <Text preset="header" text="Solo unos pasos mas..." style={{ marginBottom: spacing[3] }} />

            <View style={ROW}>
              <Text text="Tarjeta" style={ROW_LABEL} />
              <Picker
                selectedValue={"bancolombia"}
                style={ROW_INPUT}
                items={tarjeta}
              />
            </View>
            <View style={ROW}>
              <Text text="Interés %ea" style={ROW_LABEL} />
              <TextField
                placeholder="1.0%"
                preset="form"
                style={ROW_INPUT}
              />
            </View>

            <View style={ROW}>
              <Text text="Fecha de corte" style={ROW_LABEL} />
              <TextField
                placeholder="///"
                preset="form"
                style={ROW_INPUT}
              />
            </View>
            <View style={ROW}>
              <Text text="Cuota de manejo" style={ROW_LABEL} />
              <TextField
                placeholder="$XXX.XXX"
                preset="form"
                style={ROW_INPUT}
              />
            </View>

            <Text text="Tus responsabilidades son:" />
            <View style={{ flexGrow: 1, backgroundColor: color.palette.lightGrey }}>
            </View>
            <Button
              onPress={this._clickButton}
              text="Siguiente"
              style={{ marginTop: spacing[2] }}
            />
          </Screen>
        </SafeAreaView>
      </View>
    )
  }
}
