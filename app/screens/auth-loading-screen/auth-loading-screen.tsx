import * as React from "react"
import { observer } from "mobx-react"
import { ViewStyle } from "react-native"
import { Text } from "../../components/text"
import { Screen } from "../../components/screen"
import { color } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { delay } from "../../utils/delay";
import { Auth } from "aws-amplify";

export interface AuthLoadingScreenProps extends NavigationScreenProps<{}> {
}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
}

// @inject("mobxstuff")
@observer
export class AuthLoadingScreen extends React.Component<AuthLoadingScreenProps, {}> {

  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    var self = this
    // const userToken = await AsyncStorage.getItem('userToken');

    const t = await delay(3000)
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    // this.props.navigation.navigate(token ? 'Tabs' : 'Auth');
    Auth.currentAuthenticatedUser({
      bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
    }).then(user => self.props.navigation.navigate('primaryStack'))
      .catch(err => self.props.navigation.navigate('authStack'))

  };

  render() {
    return (
      <Screen style={ROOT} preset="scroll">
        <Text preset="header" tx="authLoadingScreen.header" />
      </Screen>
    )
  }
}
