import * as React from "react"
import { observer } from "mobx-react"
import { observable } from 'mobx'
import { ViewStyle, TextStyle, View, StatusBar, SafeAreaView } from "react-native"
import { Text } from "../../components/text"
import { Button } from "../../components/button"
import { Screen } from "../../components/screen"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Wallpaper } from "../../components/wallpaper";
import { Header } from "../../components/header";


export interface MainScreenProps extends NavigationScreenProps<{}> {
}

/**
 * Styles
 */

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  // backgroundColor: color.palette.black,
  paddingHorizontal: spacing[4] * 3,
}

const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[5] - 1,
  paddingHorizontal: 0,
}

const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}

const BUTTON: ViewStyle = {
  height: 50,
  marginVertical: spacing[3],
}

// const BUTTON_TEXT: TextStyle = {
//   fontSize: 15
// }


/**
 * Screen Component
 */

// @inject("mobxstuff")
@observer
export class MainScreen extends React.Component<MainScreenProps, {}> {
  // Reactive variables (Mobx)
  @observable count = 1

  _goBack = () => this.props.navigation.goBack(null)

  _clickButton = () => this.count++

  /**
   * function for optain data from API (is a React service)
   */

  render() {

    return (
      <View style={FULL}>
        <StatusBar barStyle="light-content" />
        <Wallpaper />
        <SafeAreaView style={FULL}>
          <Screen style={CONTAINER} backgroundColor={color.transparent} preset="fixed">
            <Header
              headerText="Inicio"
              leftIcon="back"
              onLeftPress={this._goBack}
              style={HEADER}
              titleStyle={HEADER_TITLE}
            />

            <Text preset="header" text="¿Qué quieres hacer" style={{ marginBottom: spacing[5], fontSize: 45 }} />
            <Text preset="secondary" text="Escoge una opción" style={{ marginBottom: spacing[5], textAlign: "center" }} />

            <Button style={BUTTON}
              onPress={() => this.props.navigation.navigate("credit")}
              text="Controlar mis pagos"
              // textStyle={BUTTON_TEXT}
            />
            <Button style={BUTTON}
              onPress={() => this.props.navigation.navigate("advices")}
              text="Consejo de compra"
              // textStyle={BUTTON_TEXT}
            />
            <Button style={BUTTON}
              onPress={() => this.props.navigation.navigate("projection")}
              text="Venta de cartera"
              // textStyle={BUTTON_TEXT}
            />
          </Screen>
        </SafeAreaView>
      </View>
    )
  }
}
