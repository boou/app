import * as React from "react"
import { observer } from "mobx-react"
import { observable } from 'mobx'
import { ViewStyle, TextStyle, View, StatusBar, SafeAreaView } from "react-native"
import { Text } from "../../components/text"
import { Button } from "../../components/button"
import { Screen } from "../../components/screen"
import { color, spacing, borderWidth } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Wallpaper } from "../../components/wallpaper";
import { Header } from "../../components/header";
import { Picker } from "../../components/picker";
import RNTesseractOcr from 'react-native-tesseract-ocr';

export interface CreditScreenProps extends NavigationScreenProps<{}> {
}

/**
 * Styles
 */

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  // backgroundColor: color.palette.black,
  paddingHorizontal: spacing[4],
}

const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[5] - 1,
  paddingHorizontal: 0,
}

const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}

/**
 * Screen Component
 */

// @inject("mobxstuff")
@observer
export class CreditScreen extends React.Component<CreditScreenProps, {}> {
  // Reactive variables (Mobx)
  @observable count = 1

  _goBack = () => this.props.navigation.goBack(null)

  /**
   * function for optain data from API (is a React service)
   */
  _getData() {
    let data = [
      { label: "bancolombia", value: "Bancolombia" },
      { label: "itau", value: "Itaú" }
    ]
    return data
  }


  render() {

    const data = this._getData()

    return (
      <View style={FULL}>
        <StatusBar barStyle="light-content" />
        <Wallpaper />
        <SafeAreaView style={FULL}>
          <Screen style={CONTAINER} backgroundColor={color.transparent} preset="fixed">

            <Header
              headerText="Crédito"
              leftIcon="back"
              onLeftPress={this._goBack}
              style={HEADER}
              titleStyle={HEADER_TITLE}
            />

            <Text preset="header" text="Tarjetas de Crédito" style={{ marginBottom: spacing[4] }} />
            <Text preset="secondary" text="Para poder apoyarte con el control de tus tarjetas de credito necesitaremos saber un poco mas de tu información" />

            <View style={{ flexDirection: "row", marginVertical: spacing[3] }}>
              <Text text="Tarjeta de crédito:" style={{ flexGrow: 1 }} />
              <Picker
                selectedValue={"bancolombia"}
                style={{width: 150, borderColor: color.palette.black, borderWidth: borderWidth[1] }}
                items={data}
              />
            </View>
            <View style={{ flexGrow: 1, backgroundColor: "#000" }}>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-around", marginVertical: spacing[3] }}>
              <Button
                text="Otra tarjeta"
              />
              <Button
                text="Terminar"
                onPress={() => this.props.navigation.navigate("detailed")}
              />
            </View>

          </Screen>
        </SafeAreaView>
      </View>
    )
  }
}
