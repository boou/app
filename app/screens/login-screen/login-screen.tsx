import * as React from "react"
import { observer } from "mobx-react"
import { observable } from 'mobx'
import { ViewStyle, TextStyle, View, StatusBar, SafeAreaView, Image, ImageStyle } from "react-native"
import { Text } from "../../components/text"
import { Button } from "../../components/button"
import { Screen } from "../../components/screen"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Wallpaper } from "../../components/wallpaper";
import { Header } from "../../components/header";
import { TextField } from "../../components/text-field";
import { Auth } from "aws-amplify";


const defaultImage = require("./logo.png")

export interface LoginScreenProps extends NavigationScreenProps<{}> {
}

/**
 * Styles
 */

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  // backgroundColor: color.palette.black,
  paddingHorizontal: spacing[4],
  alignItems: "center"
}


const BANNER_IMAGE: ImageStyle = {
  width: 210,
  height: 150,
  marginBottom: spacing[5]
}

const INPUT: ViewStyle = {
  width: '50%',
}

/**
 * Screen Component
 */

// @inject("mobxstuff")
@observer
export class LoginScreen extends React.Component<LoginScreenProps, {}> {
  @observable username = ""
  @observable password = ""
  @observable errorMessage = ""

  _loginAsync = async () => {

    //TODO: usar el formularion de autenticación
    var loginOptions = {
      username: "+57" + this.username,
      password: this.password
    }

    try {
      const sucess = await Auth.signIn(loginOptions)
      this.errorMessage = ""
    }
    catch (err) {
      console.tron.log(err);
      if (err.code === 'UserNotConfirmedException') {
        // The error happens if the user didn't finish the confirmation step when signing up
        // In this case you need to resend the code and confirm the user
        // About how to resend the code and confirm the user, please check the signUp part
        this.errorMessage = "loginScreen.userNotConfirmed"
      } else if (err.code === 'PasswordResetRequiredException') {
        // The error happens when the password is reset in the Cognito console
        // In this case you need to call forgotPassword to reset the password
        // Please check the Forgot Password part.
        this.errorMessage = "loginScreen.passwordResetRequired"
      } else if (err.code === 'NotAuthorizedException') {
        // The error happens when the incorrect password is provided
        this.errorMessage = "loginScreen.notAuthorized"
      } else if (err.code === 'UserNotFoundException') {
        // The error happens when the supplied username/email does not exist in the Cognito user pool
        this.errorMessage = "loginScreen.userNotFound"

      } else {
        this.errorMessage = "loginScreen.unkownError"
      }
      return
    }
    // const currentSession = await Auth.currentSession()
    // await AsyncStorage.setItem('userToken', currentSession.getIdToken().getJwtToken());
    this.props.navigation.navigate('primaryStack');
  };


  render() {

    return (
      <View style={FULL}>
        <StatusBar barStyle="light-content" />
        <Wallpaper />
        <SafeAreaView style={FULL}>
          <Screen style={CONTAINER} backgroundColor={color.transparent} preset="fixed">
            {/* <Text preset="header" text="BOOU" /> */}
            <Image source={defaultImage} style={BANNER_IMAGE} />
            <Text tx={this.errorMessage} preset="bold" style={{ color: color.palette.red } as TextStyle} />
            <TextField style={INPUT}
              preset="login"
              onChangeText={(text) => this.username = text}
              label="Teléfono"
              placeholder="311XXXXXXXX"
            />
            <TextField style={INPUT}
              preset="login"
              onChangeText={(text) => this.password = text}
              label="Contraseña"
              placeholder="****"
              secureTextEntry={true}
            />
            <Button
              onPress={this._loginAsync}
              text="Ingresar"
            />
            <Text text="registrarse aquí" preset="secondary" />
          </Screen>
        </SafeAreaView>
      </View>
    )
  }
}
