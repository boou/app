// tslint:disable
// this is an auto generated file. This will be overwritten

export const createAccount = `mutation CreateAccount($input: CreateAccountInput!) {
  createAccount(input: $input) {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const updateAccount = `mutation UpdateAccount($input: UpdateAccountInput!) {
  updateAccount(input: $input) {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const deleteAccount = `mutation DeleteAccount($input: DeleteAccountInput!) {
  deleteAccount(input: $input) {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const createCredit = `mutation CreateCredit($input: CreateCreditInput!) {
  createCredit(input: $input) {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const updateCredit = `mutation UpdateCredit($input: UpdateCreditInput!) {
  updateCredit(input: $input) {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const deleteCredit = `mutation DeleteCredit($input: DeleteCreditInput!) {
  deleteCredit(input: $input) {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const createBank = `mutation CreateBank($input: CreateBankInput!) {
  createBank(input: $input) {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const updateBank = `mutation UpdateBank($input: UpdateBankInput!) {
  updateBank(input: $input) {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const deleteBank = `mutation DeleteBank($input: DeleteBankInput!) {
  deleteBank(input: $input) {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const createInterest = `mutation CreateInterest($input: CreateInterestInput!) {
  createInterest(input: $input) {
    id
    value
  }
}
`;
export const updateInterest = `mutation UpdateInterest($input: UpdateInterestInput!) {
  updateInterest(input: $input) {
    id
    value
  }
}
`;
export const deleteInterest = `mutation DeleteInterest($input: DeleteInterestInput!) {
  deleteInterest(input: $input) {
    id
    value
  }
}
`;
