// tslint:disable
// this is an auto generated file. This will be overwritten

export const getAccount = `query GetAccount($id: ID!) {
  getAccount(id: $id) {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const listAccounts = `query ListAccounts(
  $filter: ModelAccountFilterInput
  $limit: Int
  $nextToken: String
) {
  listAccounts(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      cc
      phone
      credits {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getCredit = `query GetCredit($id: ID!) {
  getCredit(id: $id) {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const listCredits = `query ListCredits(
  $filter: ModelCreditFilterInput
  $limit: Int
  $nextToken: String
) {
  listCredits(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      type
      name
      bank {
        nextToken
      }
      numCuotas
      beginDate
      expireDate
      anualInterest
      cutDate
      cuotaManejo
    }
    nextToken
  }
}
`;
export const getBank = `query GetBank($id: ID!) {
  getBank(id: $id) {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const listBanks = `query ListBanks(
  $filter: ModelBankFilterInput
  $limit: Int
  $nextToken: String
) {
  listBanks(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      country
      interest {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getInterest = `query GetInterest($id: ID!) {
  getInterest(id: $id) {
    id
    value
  }
}
`;
export const listInterests = `query ListInterests(
  $filter: ModelInterestFilterInput
  $limit: Int
  $nextToken: String
) {
  listInterests(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      value
    }
    nextToken
  }
}
`;
