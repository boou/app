// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateAccount = `subscription OnCreateAccount {
  onCreateAccount {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const onUpdateAccount = `subscription OnUpdateAccount {
  onUpdateAccount {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const onDeleteAccount = `subscription OnDeleteAccount {
  onDeleteAccount {
    id
    name
    cc
    phone
    credits {
      items {
        id
        type
        name
        numCuotas
        beginDate
        expireDate
        anualInterest
        cutDate
        cuotaManejo
      }
      nextToken
    }
  }
}
`;
export const onCreateCredit = `subscription OnCreateCredit {
  onCreateCredit {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const onUpdateCredit = `subscription OnUpdateCredit {
  onUpdateCredit {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const onDeleteCredit = `subscription OnDeleteCredit {
  onDeleteCredit {
    id
    type
    name
    bank {
      items {
        id
        name
        country
      }
      nextToken
    }
    numCuotas
    beginDate
    expireDate
    anualInterest
    cutDate
    cuotaManejo
  }
}
`;
export const onCreateBank = `subscription OnCreateBank {
  onCreateBank {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const onUpdateBank = `subscription OnUpdateBank {
  onUpdateBank {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const onDeleteBank = `subscription OnDeleteBank {
  onDeleteBank {
    id
    name
    country
    interest {
      items {
        id
        value
      }
      nextToken
    }
  }
}
`;
export const onCreateInterest = `subscription OnCreateInterest {
  onCreateInterest {
    id
    value
  }
}
`;
export const onUpdateInterest = `subscription OnUpdateInterest {
  onUpdateInterest {
    id
    value
  }
}
`;
export const onDeleteInterest = `subscription OnDeleteInterest {
  onDeleteInterest {
    id
    value
  }
}
`;
