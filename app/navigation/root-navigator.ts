import { createStackNavigator, createSwitchNavigator } from "react-navigation"
import { PrimaryNavigator } from "./primary-navigator"
import { AuthNavigator } from "./auth-navigation"
import { AuthLoadingScreen } from "../screens/auth-loading-screen";

export const RootNavigator = createSwitchNavigator(
  {
    authLoading: AuthLoadingScreen,
    authStack: { screen: AuthNavigator },
    primaryStack: { screen: PrimaryNavigator },
  },
  {
    initialRouteName: "authLoading",
  }
)
