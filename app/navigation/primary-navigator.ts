import { createStackNavigator } from "react-navigation"
import { MainScreen } from "../screens/main-screen";
import { CreditScreen } from "../screens/credit-screen";
import { AdvicesScreen } from "../screens/advices-screen";
import { ProjectionScreen } from "../screens/projection-screen";
import { SpecificDataScreen } from "../screens/specific-data-screen";

export const PrimaryNavigator = createStackNavigator(
  {
    main: { screen: MainScreen },
    credit: { screen: CreditScreen },
    advices: { screen: AdvicesScreen },
    projection: { screen: ProjectionScreen },
    detailed: { screen: SpecificDataScreen }
  },
  {
    headerMode: "none",
  },
)
