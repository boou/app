import { rem } from "./base"

export const borderRadius = {
    sm: 0.125 * rem,
    base: 0.25 * rem,
    lg: 0.5 * rem,
    xl: 0.75 * rem,
    xl2: 1 * rem,
    xl3: 1.25 * rem,
    full: 9999
  }

  export const borderWidth = [
    0,
    1,
    2,
    4,
    8
  ]